# Sports Spatialverse #



### What is Sports Spatialverse? ###

**Sports Spatialverse** is an ecosystem that encompasses real and virtual interaction with sports and physical activity.  

The term “**spatialverse**” refers to a combination of virtual reality (VR), augmented reality (AR), and spatial computing.

**Sports Spatialverse** is comprised of sports venue digital twins, a sports-oriented social network, an e-commerce marketplace, web sites, mobile apps, and VR/AR games.